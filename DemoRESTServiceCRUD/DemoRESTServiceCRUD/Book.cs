﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ServiceModel;
using System.Runtime.Serialization;
namespace DemoRESTServiceCRUD
{
    public class Book
    {
        [DataMember]
        public int BookId { get; set; }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string ISBN { get; set; }

    }
    public interface IBookRepository
    {
        List<Book> GetAllBooks();
        Book GetBookById(int id);
        Book AddNewBook(Book item);
        bool DeleteABook(int id);
        bool UpdateABook(Book item);

    }
    public class BookRepository : IBookRepository
    {
        private List<Book> books = new List<Book>();
        private int counter = 1;
        public BookRepository()
        {
            AddNewBook(new Book { Title = "C# Programing ",ISBN="24561144" });
            AddNewBook(new Book { Title = "Java Programing ", ISBN = "12244441" });
            AddNewBook(new Book { Title = "WCF Programing ", ISBN = "321515121" });
        }
        public Book AddNewBook(Book newBook)
        {
            if (newBook == null)
                throw new ArgumentNullException("newBook");
            newBook.BookId = counter++;
            books.Add(newBook);
            return newBook;
        }
        //Tra ve tat ca
        public List<Book> GetAllBooks()
        {
            return books;
        }
        //Tra ve bookID
        public Book GetBookById(int BookId)
        {
            return books.Find(b => b.BookId == BookId);
        }
        //Update
        public bool UpdateABook(Book updateBook)
        {
            if (updateBook == null)
                throw new ArgumentNullException("updateBook");
            int idx = books.FindIndex(b => b.BookId == updateBook.BookId);
            if (idx == -1) return false;
            books.RemoveAt(idx);
            books.Add(updateBook);
            return true;
        }
        //Xoa

        public bool DeleteABook(int bookId)
        {
            int idx = books.FindIndex(b => b.BookId == bookId);
            if (idx == -1) return false;
            books.RemoveAll(b => b.BookId == bookId);
            return true;
        }

    
    }
}